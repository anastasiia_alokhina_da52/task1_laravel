<?php

namespace App\Providers;

use App\Services\CurrencyGenerator;
use App\Services\CurrencyRepository;
use App\Services\CurrencyRepositoryInterface;
use App\Services\GetCurrenciesCommandHandler;
use App\Services\GetMostChangedCurrencyCommandHandler;
use App\Services\GetPopularCurrenciesCommandHandler;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CurrencyRepositoryInterface::class, function($app){
            return new CurrencyRepository((new CurrencyGenerator())->generate());
        });
        $this->app->bind(GetCurrenciesCommandHandler::class, function($app){
            return new GetCurrenciesCommandHandler($this->app->make(CurrencyRepositoryInterface::class));
        });
        $this->app->bind(GetMostChangedCurrencyCommandHandler::class, function($app){
            return new GetMostChangedCurrencyCommandHandler($this->app->make(CurrencyRepositoryInterface::class));
        });
        $this->app->bind(GetPopularCurrenciesCommandHandler::class, function($app){
            return new GetPopularCurrenciesCommandHandler($this->app->make(CurrencyRepositoryInterface::class));
        });
    }
}
