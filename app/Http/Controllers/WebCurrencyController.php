<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 2/3/2019
 * Time: 12:22 PM
 */

namespace App\Http\Controllers;


use App\Services\CurrencyPresenter;
use App\Services\GetPopularCurrenciesCommandHandler;

class WebCurrencyController extends Controller
{
    public function popular()
    {
        $currencies = (resolve(GetPopularCurrenciesCommandHandler::class))->handle();
        $currencies = CurrencyPresenter::presentArray($currencies);
        return view('popular_currencies', ['currencies' => $currencies]);
    }
}