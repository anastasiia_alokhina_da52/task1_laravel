<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 2/3/2019
 * Time: 12:21 PM
 */

namespace App\Http\Controllers;


use App\Services\CurrencyPresenter;
use App\Services\GetCurrenciesCommandHandler;
use App\Services\GetMostChangedCurrencyCommandHandler;

class ApiCurrencyController extends Controller
{
    public function all()
    {
        $currencies = (resolve(GetCurrenciesCommandHandler::class))->handle();
        $currencies = CurrencyPresenter::presentArray($currencies);
        return response($currencies)
            ->header('Content-Type', 'application/json');
    }

    public function unstable()
    {
        $currency = (resolve(GetMostChangedCurrencyCommandHandler::class))->handle();
        $currency = CurrencyPresenter::present($currency);
        return response($currency)
            ->header('Content-Type', 'application/json');
    }
}