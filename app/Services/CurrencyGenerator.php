<?php

namespace App\Services;

class CurrencyGenerator
{
    public static function generate(): array
    {
        return [
            new Currency(1, 'bitcoin', 3512.25, 'bitcoin.img', 0.74),
            new Currency(2, 'xrp', 0.308, 'xrp.img', 0.02),
            new Currency(3, 'litecoin', 829.509, 'litecoin.img', 4.06),
        ];
    }
}