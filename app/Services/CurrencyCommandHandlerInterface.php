<?php

namespace App\Services;

interface CurrencyCommandHandlerInterface
{
    public function __construct(CurrencyRepositoryInterface $repository);

    public function handle();
}