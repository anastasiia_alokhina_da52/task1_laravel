<?php

namespace App\Services;

class GetMostChangedCurrencyCommandHandler extends AbstractCurrencyCommandHandler
{
    public function handle(): Currency
    {
        $currencies = $this->repository->findAll();
        $sortedCurrencies = $this->sortCurrenciesBy($currencies, 'dailyChangePercent');
        return $sortedCurrencies[0];
    }
}