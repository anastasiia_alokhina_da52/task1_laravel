<?php

namespace App\Services;

class GetPopularCurrenciesCommandHandler extends AbstractCurrencyCommandHandler
{
    const POPULAR_COUNT = 3;

    public function handle(int $count = self::POPULAR_COUNT): array
    {
        $currencies = $this->repository->findAll();
        $sortedCurrencies = $this->sortCurrenciesBy($currencies, 'price');
        return array_slice($sortedCurrencies, 0, self::POPULAR_COUNT);
    }
}