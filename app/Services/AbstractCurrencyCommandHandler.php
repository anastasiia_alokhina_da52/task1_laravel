<?php

namespace App\Services;

abstract class AbstractCurrencyCommandHandler implements CurrencyCommandHandlerInterface
{
    protected $repository;

    public function __construct(CurrencyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    protected function sortCurrenciesBy(array $currencies, string $field)
    {
        usort($currencies, function ($a, $b) use ($field) {
            return call_user_func([$a, 'get' . ucfirst($field)]) < call_user_func([$b, 'get' . ucfirst($field)]);
        });
        return $currencies;
    }
}