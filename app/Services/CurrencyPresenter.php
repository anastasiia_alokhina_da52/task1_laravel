<?php

namespace App\Services;

class CurrencyPresenter
{
    public static function present(Currency $currency): array
    {
        $presentedCurrency['id'] = $currency->getId();
        $presentedCurrency['name'] = $currency->getName();
        $presentedCurrency['img'] = $currency->getImageUrl();
        $presentedCurrency['price'] = $currency->getPrice();
        $presentedCurrency['daily_change'] = $currency->getDailyChangePercent();
        return $presentedCurrency;
    }

    public static function presentArray(array $currencies): array
    {
        $presentedCurrencies = [];
        foreach ($currencies as $currency){
            $presentedCurrencies[] = self::present($currency);
        }
        return $presentedCurrencies;
    }
}