<?php

namespace App\Services;

class GetCurrenciesCommandHandler extends AbstractCurrencyCommandHandler
{
    public function handle(): array
    {
        return $this->repository->findAll();
    }
}