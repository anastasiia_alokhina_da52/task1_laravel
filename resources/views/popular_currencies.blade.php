<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Popular Currencies</title>
</head>
<body>
<h1>Popular Currencies: </h1>
<ul>
    @foreach($currencies as $currency)
    <li>
        @foreach($currency as $field => $value)
            <p>
                <b>{{$field}}: </b>{{$value}}
                <br>
            </p>
        @endforeach
    </li>
    @endforeach
</ul>
</body>
</html>